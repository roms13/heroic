package info.kroms.playground;

import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.flex.remoting.RemotingInclude;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * <b>Spring BlazeDS Annotations</b>
 * <p>
 * The annotation Service is used to tell Spring this object is a service bean.
 * The annotation RemotingDestination is used to expose it as a Flex Remoting destination.
 * The annotation RemotingInclude above individual methods is used to expose public methods of the service to Flex
 * as opposed to RemotingExclude which hides them from a Flex client.
 * </p>
 *
 * <b>Jersey JAX-RS Annotations</b>
 * <p>
 * The annotation Path is used to define the base URL pattern (after the app server's context) to map to this RESTful Jersey service.
 * The annotation GET above individual methods is used to define the HTTP action/verb associated with the service method.
 * The annotation Produces above individual methods is used to define the request and response types for the service method.
 * The annotation GET Path("{id}") above individual methods is used to add specific method parameters for the service method via REST path.
 * </p>
 */

//déclare le bean comme étant managé par spring
@Service("info.kroms.playground.WelcomeService") // BLAZEDS
// déclare le bean comme étant accessible par Flex
@RemotingDestination(channels={"playground-amf"},value="welcomeService") // BLAZEDS
@Path("/welcome") // JERSEY
public class WelcomeService
{
    @RemotingInclude // BLAZEDS
    @GET // JERSEY
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML }) // JERSEY
    @Path("/hello/{person}") // JERSEY
    public String hello( @PathParam("person") String person) {
        return "Bienvenue à toi " + person;
    }
}