package info.kroms.bean;

import java.util.Date;

/**
 * Created by Romain KELIFA on 03/04/2016.
 */
public class Contact {
    private String uid;
    private String firstname;
    private String lastname;
    private Date birthdate;

    public Contact(){}

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
}
